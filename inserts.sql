insert into pizze values (default, 'Rural', 15.70);
insert into pizze values (default, 'Uncle Zbyszek', 21.50);
insert into pizze values (default, 'Underwater', 20);
insert into pizze values (default, 'None pizza with pineapple', 10.50);
insert into pizze values (default, 'Neck of the giraffe', 14.42);
insert into pizze values (default, 'Q-dot', 13.37);

insert into skladniki values ('pineapple'), ('uncle Zbyszek'), ('squid'), ('gouda cheese'), ('mozzarella cheese'), ('JAGNA cheese');

insert into klienci values ('Zbyszek', 'Wujek', 'Ul. Heweliowa 21, 03-539 Warszawa'), ('Algorytmik', 'Cormen', 'Al. Bezdrzew 37, 10-241 Suwałki'), ('Kylo', 'Ren', 'ul. Galaktyczna, 80-299 Gdańsk'), ('Ben', 'Solo', 'ul. Galaktyczna, 80-299 Gdańsk');

insert into pizzeskladniki values (1, 6), (1, 4), (2, 4), (2, 2), (3, 3), (4, 1), (5, 4), (5, 5), (5, 6), (6, 1), (6, 3), (6, 5);


