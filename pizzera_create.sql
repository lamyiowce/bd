-- Created by Vertabelo (http://vertabelo.com)
-- Last modification date: 2018-01-07 21:34:21.32

-- tables
-- Table: Klienci
CREATE TABLE Klienci (
    Imie text  NOT NULL,
    Nazwisko text  NOT NULL,
    Adres text  NOT NULL,
    Id serial  NOT NULL,
    CONSTRAINT Klienci_pk PRIMARY KEY (Id)
);

-- Table: Pizze
CREATE TABLE Pizze (
    Id serial  NOT NULL,
    Nazwa text  NOT NULL,
    Cena numeric  NOT NULL,
    CONSTRAINT Pizze_pk PRIMARY KEY (Id)
);

-- Table: PizzeSkladniki
CREATE TABLE PizzeSkladniki (
    Pizze_Id serial  NOT NULL,
    Skladniki_Id serial  NOT NULL,
    CONSTRAINT PizzeSkladniki_pk PRIMARY KEY (Pizze_Id,Skladniki_Id)
);

-- Table: Skladniki
CREATE TABLE Skladniki (
    Nazwa text  NOT NULL,
    Id serial  NOT NULL,
    CONSTRAINT Skladniki_pk PRIMARY KEY (Id)
);

-- Table: Zamowienie
CREATE TABLE Zamowienie (
    Id serial  NOT NULL,
    czy_oplacono boolean  NOT NULL,
    Klienci_Id serial  NOT NULL,
    CONSTRAINT Zamowienie_pk PRIMARY KEY (Id)
);

-- Table: Zawartosc_zamowienia
CREATE TABLE Zawartosc_zamowienia (
    liczba int  NOT NULL,
    Zamowienie_Id serial  NOT NULL,
    Pizze_id serial  NOT NULL,
    CONSTRAINT Zawartosc_zamowienia_pk PRIMARY KEY (Zamowienie_Id,Pizze_id)
);

-- foreign keys
-- Reference: PizzeSkladniki_Pizze (table: PizzeSkladniki)
ALTER TABLE PizzeSkladniki ADD CONSTRAINT PizzeSkladniki_Pizze
    FOREIGN KEY (Pizze_Id)
    REFERENCES Pizze (Id)  
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE
;

-- Reference: PizzeSkladniki_Skladniki (table: PizzeSkladniki)
ALTER TABLE PizzeSkladniki ADD CONSTRAINT PizzeSkladniki_Skladniki
    FOREIGN KEY (Skladniki_Id)
    REFERENCES Skladniki (Id)  
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE
;

-- Reference: Zamowienie_Klienci (table: Zamowienie)
ALTER TABLE Zamowienie ADD CONSTRAINT Zamowienie_Klienci
    FOREIGN KEY (Klienci_Id)
    REFERENCES Klienci (Id)  
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE
;

-- Reference: Zawartosc_zamowienia_Pizze (table: Zawartosc_zamowienia)
ALTER TABLE Zawartosc_zamowienia ADD CONSTRAINT Zawartosc_zamowienia_Pizze
    FOREIGN KEY (Pizze_id)
    REFERENCES Pizze (Id)  
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE
;

-- Reference: Zawartosc_zamowienia_Zamowienie (table: Zawartosc_zamowienia)
ALTER TABLE Zawartosc_zamowienia ADD CONSTRAINT Zawartosc_zamowienia_Zamowienie
    FOREIGN KEY (Zamowienie_Id)
    REFERENCES Zamowienie (Id)  
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE
;

-- End of file.

