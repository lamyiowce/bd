
SELECT * FROM (SELECT id FROM pizze LIMIT 1) AS pizze_id, (SELECT * FROM skladniki ORDER BY random() LIMIT n) AS skladniki_id;


INSERT INTO pizze (id, cena, nazwa) VALUES (DEFAULT, cena, nazwa_pizzy) RETURNING id 

SELECT * FROM (
	INSERT INTO pizze (id, cena, nazwa) VALUES (DEFAULT, cena, nazwa_pizzy) RETURNING id
	) AS pizze_id, 
	(
	SELECT id FROM skladniki ORDER BY random() LIMIT 3
	) 
	AS skladniki_id;


CREATE OR REPLACE FUNCTION ulepsz_pizze() RETURNS void AS $$
DECLARE
	id_ananasa integer;
BEGIN 
	SELECT skladniki.id INTO id_ananasa FROM skladniki WHERE nazwa = 'ananas';
	INSERT INTO pizzeskladniki
		SELECT pizze_id, 6 FROM pizzeskladniki GROUP BY pizze_id HAVING BOOL_OR	(skladniki_id = 6) = false;
END;	
$$ LANGUAGE plpgsql;
	

CREATE OR REPLACE FUNCTION oplac_zamowienie(id_zamowienia integer) RETURNS void AS $$
	UPDATE zamowienie SET czy_oplacono = true WHERE id = 'id_zamowienia';
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION koszta_zamowien() RETURNS TABLE(id_zamowienia integer, id_klienta integer, nazwisko text, czy_oplacono boolean, suma numeric) AS $$ 
BEGIN
	RETURN QUERY SELECT zamowienie.id AS id_zamowienia, klienci.id AS id_klienta, klienci.nazwisko, zamowienie.czy_oplacono, COALESCE(koszt_pizz, 0) AS koszt_suma FROM zamowienie LEFT JOIN (SELECT zamowienie_id, SUM(cena*liczba) AS koszt_pizz FROM zawartosc_zamowienia JOIN pizze ON zawartosc_zamowienia.pizze_id = pizze.id GROUP BY zawartosc_zamowienia.zamowienie_id) AS a ON zamowienie.id = a.zamowienie_id JOIN klienci ON zamowienie.klienci_id = klienci.id;
END
$$ LANGUAGE plpgsql;
