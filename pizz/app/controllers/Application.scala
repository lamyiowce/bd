package controllers

import helpers.ActionWithAwait
import models._
import play.api.data.{Form, Mapping}
import play.api.data.Forms._
import play.api.mvc._
import slick.jdbc.JdbcBackend.Database

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.concurrent.duration._


object Application extends Controller {

  val timeout: FiniteDuration = 100 seconds
  val db = Database.forURL("jdbc:postgresql://localhost:5432/bd", "jb385134", "x")

  val orderForm: Form[(Order, List[(Option[Int], Int)])] = Form(
    tuple(
      "order" -> mapping(
        "id" -> optional(number),
        "paid" -> default(boolean, false),
        "clientId" -> number
      )(Order.apply)(Order.unapply),
      "pizzaQuants" -> list(
        tuple(
          "pizzaId" -> optional(number),
          "count" -> number
        )
      )
    )
  )

  val setPaidForm: Form[(Boolean, List[(Option[Int], Boolean)])] = Form(
    tuple(
      "setPaid" -> boolean,
      "payList" -> list(
        tuple(
          "orderId" -> optional(number),
          "setPaid" -> boolean
        )
      )
    )
  )

  private val ingredientMapping: Mapping[Ingredient] = mapping(
    "name" -> nonEmptyText,
    "id" -> optional(number)
  )(Ingredient.apply)(Ingredient.unapply)

  private val pizzaMapping: Mapping[Pizza] = mapping(
    "id" -> optional(number),
    "name" -> nonEmptyText,
    "price" -> bigDecimal(10, 2)
  )((id, name, price) => Pizza(id, name, price.doubleValue())){case Pizza(id, name, price) => Some(id, name, BigDecimal(price))}

  val randomPizzaForm: Form[(Pizza, Int)] = Form(
    tuple(
      "pizza" -> pizzaMapping,
      "count" -> number
    )
  )

  val pizzaWithIngsForm: Form[(Pizza, List[(Ingredient, Boolean)])] = Form(
    tuple(
      "pizza" -> pizzaMapping,
      "ings" -> list(
        tuple(
          "ing" -> ingredientMapping,
          "isIn" -> boolean
        )
      )
    )
  )

  val clientForm: Form[Client] = Form(
    mapping(
      "name" -> nonEmptyText,
      "surname" -> nonEmptyText,
      "address" -> nonEmptyText,
      "id" -> optional(number)
    )(Client.apply)(Client.unapply)
  )

  def index = Action {
    Ok(views.html.index(""))
  }

  def placeOrder = ActionWithAwait {
    db.run(Pizzas.listWithIngredients zip Clients.list)
        .map { case (pizzas, clients) =>
            val pizzasWithIngs = pizzas.groupBy(_._1).map { case (p, pis) => (p, pis.map(_._2)) }.toSeq
            Ok(views.html.placeOrder(pizzasWithIngs, clients, orderForm.fill((Order(None, false, -1), pizzas.map{p => (p._1.id, 0)}.toList))))
        }
  }

  def saveOrder = ActionWithAwait { implicit request =>
      orderForm.bindFromRequest.fold(
      errors => {
        db.run(Pizzas.listWithIngredients zip Clients.list)
          .map{ case (pizzas, clients) =>
            Ok(views.html.placeOrder(
              pizzas.groupBy(_._1).map{ case (p, pis) => (p, pis.map(_._2)) }.toSeq,
              clients,
              errors
            )) }
      },
      order => {
        db.run(Orders.insert(order._1, order._2)).map(_ => Ok(views.html.index(s"Your order has been saved.")))
      }
    )
  }

  def listOrders = ActionWithAwait {
      db.run(Orders.search(OrderSearchData(None, None, None))).map{
        orders =>
          val os = orders.groupBy(_._1).toSeq.map{ case(order, seq) => (order, seq.head._3, seq.map(_._2))}.sortBy(_._1.id)
          Ok(views.html.listOrders(os, setPaidForm.fill((false, os.map{case (o, _, _) => (o.id, false)}.toList))))
      }
  }

  def setPaid = ActionWithAwait { implicit request =>
    setPaidForm.bindFromRequest.fold(
      errors => {
        db.run(Orders.search(OrderSearchData(None, None, None))).map {
          orders =>
            val os = orders.groupBy(_._1).toSeq.map { case (order, seq) => (order, seq.head._3, seq.map(_._2)) }
            Ok(views.html.listOrders(os, errors))
        }
      },
      payList => {
        db.run(Orders.setPaid(payList._2.filter(_._2).map(_._1), payList._1) zip Orders.search(OrderSearchData(None, None, None)))
          .map {
            case (_, orders) =>
              val os = orders.groupBy(_._1).toSeq.map { case (order, seq) => (order, seq.head._3, seq.map(_._2)) }.sortBy(_._1.id)
              Ok(views.html.listOrders(os, setPaidForm.fill((false, os.map { case (o, _, _) => (o.id, false) }.toList))))
          }
      }
    )
  }

  def listPizzas = ActionWithAwait { implicit request =>
    db.run(Pizzas.listWithIngredients).map{ pizzas =>
      Ok(views.html.listPizzas(pizzas.groupBy(_._1).map{ case(pizza, ings) => (pizza, ings.map(_._2))}.toSeq))
    }
  }

  def editPizza(id: Int) = ActionWithAwait { implicit request =>
    db.run(Pizzas.get(id) zip Ingredients.list zip Ingredients.getIngsForPizzas(id)).map{
      case ((Some(pizza), allIngs), pizzaIngs) =>
        val ingredientsIn = allIngs.map{ ing => (ing, pizzaIngs.contains(ing))}.toList
        Ok(views.html.editPizza(pizzaWithIngsForm.fill((pizza, ingredientsIn))))
    }
  }

  def savePizza = ActionWithAwait { implicit request =>
    pizzaWithIngsForm.bindFromRequest.fold(
      errors => {
        Future(BadRequest(views.html.editPizza(errors)))
      },
      pizza => {
        db.run(Pizzas.upsert(pizza._1, pizza._2.filter(_._2).map(_._1.id.get)))
          .map{ _ => Redirect(routes.Application.listPizzas())}
      }
    )
  }

  def newPizza = ActionWithAwait {
    db.run(Ingredients.list).map{
      ings =>
        Ok(views.html.editPizza(pizzaWithIngsForm.fill((Pizza(None, "", 0), ings.map(x => (x,false)).toList))))
    }
  }

  def saveRandom = ActionWithAwait { implicit request =>
    randomPizzaForm.bindFromRequest.fold(
      errors =>
        Future(BadRequest(views.html.randomPizza(errors))),
      success =>
        db.run(Pizzas.addRandom(success._1, success._2)).map{ _ =>
          Redirect(routes.Application.listPizzas())
        }
    )
  }

  def newRandom = Action {
    Ok(views.html.randomPizza(randomPizzaForm))
  }

  def listClients = ActionWithAwait {
    db.run(Clients.list).map(clients => Ok(views.html.listClients(clients)))
  }

  def editClient(id: Int) = ActionWithAwait {
    db.run(Clients.get(id)).map(client => {
      Ok(views.html.editClient(clientForm.fill(client)))
    }
    )
  }

  def saveClient = ActionWithAwait { implicit request =>
    clientForm.bindFromRequest.fold(
      errors => Future(BadRequest(views.html.editClient(errors))),
      success => db.run(Clients.upsert(success)).map{ _ => Redirect(routes.Application.listClients)}
    )
  }

  def newClient = Action {
    Ok(views.html.editClient(clientForm))
  }
}
