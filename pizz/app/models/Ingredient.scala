package models

import slick.jdbc.H2Profile.api._
case class Ingredient(name: String, id: Option[Int]) {
  override def toString = name

}

import slick.jdbc.GetResult

class Ingredients(tag: Tag) extends Table[Ingredient](tag, "skladniki") {
  def id = column[Option[Int]]("id", O.PrimaryKey)
  def name = column[String]("nazwa")

  def * = (name, id) <> (Ingredient.tupled, Ingredient.unapply)

}

object Ingredients {
  val ingredients = TableQuery[Ingredients]

  implicit def getIngredientResult = GetResult(r => Ingredient(r.nextString, r.nextIntOption))

  def list =
    sql"""SELECT * FROM skladniki""".as[Ingredient]

  def getIngsForPizzas(id: Int): DBIO[Seq[Ingredient]] =
    sql"""SELECT nazwa, id FROM pizzeskladniki
         JOIN skladniki ON skladniki_id = id
         WHERE pizze_id = $id
       """
      .as[Ingredient]
}
