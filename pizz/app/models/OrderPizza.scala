package models

import slick.jdbc.H2Profile.api._
import slick.jdbc.GetResult

case class OrderPizza(clientId: Int, orderId: Int, count: Int)

class OrderPizzas(tag: Tag) extends Table[OrderPizza](tag, "zawartosc_zamowienia") {
  def clientId = column[Int]("zamowienie_id", O.PrimaryKey)
  def orderId = column[Int]("pizze_id", O.PrimaryKey)
  def count = column[Int]("liczba")

  def * = (clientId, orderId, count) <> (OrderPizza.tupled, OrderPizza.unapply)
}

object OrderPizzas {
  implicit val getOrderPizzasResult = GetResult(r => OrderPizza(r.nextInt, r.nextInt, r.nextInt))
}