package models

import slick.jdbc.H2Profile.api._
import slick.jdbc.GetResult
import scala.concurrent.ExecutionContext.Implicits.global

case class Order(id: Option[Int], paid: Boolean, clientId: Int)

case class OrderWithList(order: Order, list: List[(Pizza, Int)])

class Orders(tag: Tag) extends Table[Order](tag, "zamowienie") {
  def id = column[Option[Int]]("id", O.PrimaryKey)
  def clientId = column[Int]("klienci_id")
  def paid = column[Boolean]("czy_oplacono")

  def * = (id, paid, clientId) <> (Order.tupled, Order.unapply)

}

object Orders {
  val orders = TableQuery[Orders]
  val orderPizzas = TableQuery[OrderPizzas]

  import Clients.getClientResult
  import Pizzas.getPizzaResult
  import OrderPizzas.getOrderPizzasResult
  implicit val getOrderResult = GetResult(r => Order(r.nextIntOption, r.nextBoolean, r.nextInt))
  implicit val getOrderPizzaClientTupleResult = GetResult(r => (getOrderResult(r), (r.nextInt, getPizzaResult(r)), getClientResult(r)))

  def insert(o: Order, list: List[(Option[Int], Int)]) : DBIO[Seq[Int]]= {
    val boolstr = if (o.paid) "TRUE" else "FALSE"
    sql"""INSERT INTO zamowienie (id, czy_oplacono, klienci_id) VALUES
          (#${o.id.getOrElse("default")},
          #${boolstr},
          ${o.clientId})
          RETURNING id
        """.as[Int]
        .headOption
        .flatMap{ orderId =>
          DBIO.sequence(list.filter(_._2 > 0).map { case (pizzaId, count) =>
            sqlu"""INSERT INTO zawartosc_zamowienia VALUES (${count}, ${orderId}, ${pizzaId.get})"""
          })
        }
  }

  def search(orderSearchData: OrderSearchData) : DBIO[Seq[(Order, (Int, Pizza), Client)]] = {
    sql"""SELECT zamowienie.id, czy_oplacono, klienci_id, liczba, pizze.id, nazwa, cena, imie, nazwisko, adres, klienci.id FROM
         zamowienie JOIN zawartosc_zamowienia ON id = zamowienie_id
         JOIN pizze ON pizze.id = pizze_id
         JOIN klienci ON klienci.id = klienci_id
         ORDER BY zamowienie.id"""
      .as[(Order, (Int, Pizza), Client)]
  }

  def setPaid(list: List[Option[Int]], toSet: Boolean) = {
    DBIO.sequence(
      list.map{ case Some(id) =>
      val boolstr = if (toSet) "TRUE" else "FALSE"
      sqlu"""UPDATE zamowienie SET czy_oplacono = #$boolstr WHERE id = $id"""
      }
    )
  }

}

case class OrderSearchData(clientId: Option[Int], clientLike: Option[String], orderBy: Option[String])