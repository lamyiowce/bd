package models

import slick.jdbc.H2Profile.api._
import slick.jdbc.GetResult

case class PizzaIngredient(pizzaId: Int, ingredientId: Int)

class PizzaIngredients(tag: Tag) extends Table[PizzaIngredient](tag, "pizze") {
  def pizzaId = column[Int]("pizze_id", O.PrimaryKey)
  def ingredientId = column[Int]("skladniki_id", O.PrimaryKey)

  def * = (pizzaId, ingredientId) <> (PizzaIngredient.tupled, PizzaIngredient.unapply)
}