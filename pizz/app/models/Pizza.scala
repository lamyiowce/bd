package models

import slick.jdbc.H2Profile.api._
import slick.jdbc.GetResult

import scala.concurrent.ExecutionContext.Implicits.global

case class Pizza(id: Option[Int], name: String, price: Double)

class Pizzas(tag: Tag) extends Table[Pizza](tag, "pizze") {
   def id = column[Option[Int]]("id", O.PrimaryKey)
   def name = column[String]("nazwa")
   def price = column[Double]("cena")

   def * = (id, name, price) <> (Pizza.tupled, Pizza.unapply)
}

object Pizzas {

  val pizzas = TableQuery[Pizzas]
  val ingredients = TableQuery[Ingredients]
  val pizIng = TableQuery[PizzaIngredients]

  import Ingredients.getIngredientResult

  implicit val getPizzaResult = GetResult(r => Pizza(r.nextIntOption, r.nextString, r.nextDouble))
  implicit val getPizzaWithIngsResult =  GetResult(r => (getPizzaResult(r), getIngredientResult(r)))
  implicit val getNothing = GetResult(r => Void())

  def list: DBIO[Seq[Pizza]] = sql"SELECT * FROM pizze".as[Pizza]

  def listWithIngredients: DBIO[Seq[(Pizza, Ingredient)]] = sql"""
      SELECT p.id, p.nazwa, p.cena, s.nazwa, s.id FROM pizze AS p
      JOIN pizzeskladniki AS ps ON p.id = ps.pizze_id
      JOIN skladniki AS s ON ps.skladniki_id = s.id
    """.as[(Pizza, Ingredient)]

  def upsert(pizza: Pizza, ings: List[Int]) = {
    pizza.id.map { id =>
      sqlu"""UPDATE pizze SET nazwa = ${pizza.name}, cena = ${pizza.price} WHERE id = ${pizza.id}"""
        .map(_ => id)
    }.getOrElse(
      sql"""INSERT INTO pizze VALUES (default, ${pizza.name}, ${pizza.price}) RETURNING id"""
        .as[Int]
        .head
    ).flatMap { pizzaId =>
      sqlu"""DELETE FROM pizzeskladniki WHERE pizze_id = ${pizzaId}""".andThen(
        DBIO.sequence(ings.map { ingId =>
          sqlu"""INSERT INTO pizzeskladniki VALUES (${pizzaId}, ${ingId})"""
        })
      )
    }
  }

  def get(id: Int): DBIO[Option[Pizza]] =
    sql"""SELECT * FROM pizze WHERE id = $id""".as[Pizza].headOption

  def addRandom(p: Pizza, count: Int) =
    sql"""SELECT dodaj_losowa_pizze(${p.name}, #${p.price}, $count)""".as[Void]
}

case class Void()

