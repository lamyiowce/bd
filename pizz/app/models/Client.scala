package models

import slick.jdbc.H2Profile.api._
import slick.jdbc.GetResult
import scala.concurrent.ExecutionContext.Implicits.global

case class Client(
                   name: String,
                   surname: String,
                   address: String,
                   id: Option[Int]
)

class Clients(tag: Tag) extends Table[Client](tag, "klienci") {
  def id = column[Option[Int]]("id", O.PrimaryKey)
  def name = column[String]("imie")
  def surname = column[String]("nazwisko")
  def address = column[String]("adres")

  def * = (name, surname, address, id) <> (Client.tupled, Client.unapply)
}

object Clients {

  val clients = TableQuery[Clients]

  implicit val getClientResult = GetResult(r => Client(r.nextString, r.nextString, r.nextString, r.nextIntOption))
  implicit val getClientWithOrderResult = GetResult(r =>
    (Client(r.nextString, r.nextString, r.nextString, r.nextIntOption), Order(r.nextIntOption, r.nextBoolean, r.nextInt)))

  def list: DBIO[Seq[Client]] = sql"SELECT * FROM klienci".as[Client]

  def get(id: Int): DBIO[Client] = sql"""SELECT * FROM klienci WHERE id = $id""".as[Client].head

  def upsert(client: Client) =
    client.id.map { id =>
      sqlu"""UPDATE klienci
            SET imie = ${client.name}, nazwisko = ${client.surname}, adres = ${client.address}
            WHERE id = ${id}"""
        .map(_ => id)
    }.getOrElse(
      sql"""INSERT INTO klienci VALUES (${client.name}, ${client.surname}, ${client.address}, default) RETURNING id"""
        .as[Int]
        .head
    )
}

