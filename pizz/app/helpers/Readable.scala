package helpers

object Readable {
  def apply(b: Boolean) = if (b) "Yes!" else "No."
}
