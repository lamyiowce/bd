package helpers

object Precision {
  def apply(d: Double, i: Int) = {
    s"%.${i}f".format(d)
  }
}
