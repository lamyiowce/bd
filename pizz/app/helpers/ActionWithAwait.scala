package helpers

import play.api.mvc.{Action, AnyContent, Request, Result}
import scala.concurrent.duration._

import scala.concurrent.{Await, Future}

object ActionWithAwait {
  val timeout = 100 seconds

  def apply(x: Request[AnyContent] => Future[Result]) =
    Action { implicit request => Await.result(x(request), timeout) }

  def apply(x: Future[Result]) =
    Action { implicit request => Await.result(x, timeout) }

}