-- Julia Bazińska 385134

-- Zaznacza, że zamowienie o danym ID jest opłacone.

CREATE OR REPLACE FUNCTION oplac_zamowienie(id_zamowienia integer) RETURNS void AS $$
	UPDATE zamowienie SET czy_oplacono = true WHERE id = 'id_zamowienia';
$$ LANGUAGE plpgsql;


-- Pokazuje informacje o zamowieniach i ich koszta.

CREATE OR REPLACE FUNCTION koszta_zamowien() RETURNS TABLE(id_zamowienia integer, id_klienta integer, nazwisko text, czy_oplacono boolean, suma numeric) AS $$ 
BEGIN
	RETURN QUERY SELECT zamowienie.id AS id_zamowienia, klienci.id AS id_klienta, klienci.nazwisko, zamowienie.czy_oplacono, COALESCE(koszt_pizz, 0) AS koszt_suma FROM zamowienie LEFT JOIN (SELECT zamowienie_id, SUM(cena*liczba) AS koszt_pizz FROM zawartosc_zamowienia JOIN pizze ON zawartosc_zamowienia.pizze_id = pizze.id GROUP BY zawartosc_zamowienia.zamowienie_id) AS a ON zamowienie.id = a.zamowienie_id JOIN klienci ON zamowienie.klienci_id = klienci.id;
END
$$ LANGUAGE plpgsql;


-- Funkcja dodająca pizzę o n losowych składnikach.

CREATE OR REPLACE FUNCTION dodaj_losowa_pizze(nazwa_pizzy text, cena_pizzy numeric(2), n integer)
	RETURNS void AS $$
DECLARE
	dodane_id integer;
BEGIN 
	INSERT INTO pizze VALUES (DEFAULT, nazwa_pizzy, cena_pizzy) RETURNING id INTO dodane_id;
	INSERT INTO pizzeskladniki 
		SELECT dodane_id, id FROM skladniki ORDER BY random() LIMIT n;

END	
$$ LANGUAGE plpgsql;	


-- Funkcja ulepszająca wszystkie pizze.

CREATE OR REPLACE FUNCTION ulepsz_pizze() RETURNS void AS $$
DECLARE
	id_ananasa integer;
BEGIN 
	SELECT skladniki.id INTO id_ananasa FROM skladniki WHERE nazwa = 'ananas';
	INSERT INTO pizzeskladniki
		SELECT pizze_id, 6 FROM pizzeskladniki GROUP BY pizze_id HAVING BOOL_OR	(skladniki_id = 6) = false;
END;	
$$ LANGUAGE plpgsql;
	

