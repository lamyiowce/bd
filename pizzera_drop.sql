-- Created by Vertabelo (http://vertabelo.com)
-- Last modification date: 2018-01-07 21:34:21.32

-- foreign keys
ALTER TABLE PizzeSkladniki
    DROP CONSTRAINT PizzeSkladniki_Pizze;

ALTER TABLE PizzeSkladniki
    DROP CONSTRAINT PizzeSkladniki_Skladniki;

ALTER TABLE Zamowienie
    DROP CONSTRAINT Zamowienie_Klienci;

ALTER TABLE Zawartosc_zamowienia
    DROP CONSTRAINT Zawartosc_zamowienia_Pizze;

ALTER TABLE Zawartosc_zamowienia
    DROP CONSTRAINT Zawartosc_zamowienia_Zamowienie;

-- tables
DROP TABLE Klienci;

DROP TABLE Pizze;

DROP TABLE PizzeSkladniki;

DROP TABLE Skladniki;

DROP TABLE Zamowienie;

DROP TABLE Zawartosc_zamowienia;

-- End of file.

